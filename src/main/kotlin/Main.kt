import org.apache.commons.math3.distribution.NormalDistribution
import org.deeplearning4j.nn.api.Model
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.api.IterationListener
import org.deeplearning4j.util.ModelSerializer
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.lossfunctions.LossFunctions
import org.nield.kotlinstatistics.median
import org.nield.kotlinstatistics.standardDeviation
import java.io.File
import java.util.*


fun macToId(mac: String): Int {
    return when (mac) {
        "30:AE:A4:05:5C:72" -> 1
        "30:AE:A4:04:82:42" -> 2
        "30:AE:A4:20:CB:6E" -> 3
        else -> 0
    }
}

data class Value(
        val macId: Int,
        val rssi: Double,
        val positionX: Double,
        val positionY: Double,
        val groupKey: String
)

fun loadData(files: List<String>): List<Value> {
    return files.flatMap {
        File(it)
                .readLines()
                .drop(1)
                .map {
                    it.split(";")
                }
                .filter {
                    it[1] in listOf("1")
                }
                .map {
                    Value(
                            macToId(it[2]),
                            it[3].toDouble(),
                            it[5].toDouble() - 0.5,
                            it[6].toDouble() - 0.5,
                            it[5] + ";" + it[6]
                    )
                }
                .filter {
                    it.macId > 0
                }
    }
}


data class TrainingRow(
        val rssi1: List<Double>,
        val rssi2: List<Double>,
        val rssi3: List<Double>,
        val positionX: Double,
        val positionY: Double
)

val random = Random()

fun normlizeRssi(rssi: Double) = (-rssi) * 0.01
fun normlizeX(x: Double) = (x + 7) / 10
fun normlizeY(x: Double) = (x + 5) / 10
fun check(v: Double): Double {
    check(v in 0..1)
    return v
}

fun firstTrainingSet(data: List<Value>): List<TrainingRow> {
    val takeRandomRssi = fun(values: List<Value>): List<Double> {
        return (1..10).map {
            check(normlizeRssi(values[random.nextInt(values.size)].rssi))
        }
    }

    val trainingRows = data.groupBy { it.groupKey }.flatMap {
        val groupsByMac = it.value.take(100).groupBy { it.macId }
        it.value.flatMap { v ->
            (1..1).map {
                TrainingRow(
                        takeRandomRssi(groupsByMac[1]!!),
                        takeRandomRssi(groupsByMac[2]!!),
                        takeRandomRssi(groupsByMac[3]!!),
                        check(normlizeX(v.positionX)),
                        check(normlizeY(v.positionY))
                )
            }
        }
    }

    return trainingRows.shuffled()
}

fun secondTrainingSet(data: List<Value>): List<TrainingRow> {
    val groups = data.groupBy { it.groupKey }
    val trainingRows = groups.flatMap {
        val groupsByMac = it.value.take(groups.minBy { it.value.size }!!.value.size).groupBy { it.macId }

        val getSamples = { id: Int ->
            val a = NormalDistribution(groupsByMac[id]!!.map { it.rssi }.median(), groupsByMac[id]!!.map { it.rssi }.standardDeviation())
            a.sample(10).toList().sorted()
        }

        it.value.flatMap { v ->
            (1..1).map {
                TrainingRow(
                        getSamples(1),
                        getSamples(2),
                        getSamples(3),
                        check(normlizeX(v.positionX)),
                        check(normlizeY(v.positionY))
                )
            }
        }
    }

    return trainingRows.shuffled()
}
fun convertToDataSet(rows: List<TrainingRow>): DataSet {


    val trainingInputs = Nd4j.zeros(rows.size, 30)
    val trainingOutputs = Nd4j.zeros(rows.size, 2)

    rows.forEachIndexed { i, trainingRow ->
        listOf(
                trainingRow.rssi1.sorted().get(trainingRow.rssi1.size / 2),
                trainingRow.rssi2.sorted().get(trainingRow.rssi2.size / 2),
                trainingRow.rssi3.sorted().get(trainingRow.rssi3.size / 2)
        ).mapIndexed { index, d ->
            trainingInputs.putScalar(intArrayOf(i, index), d)
        }

        trainingOutputs.putScalar(intArrayOf(i, 0), trainingRow.positionX)
        trainingOutputs.putScalar(intArrayOf(i, 1), trainingRow.positionY)
    }

    return DataSet(trainingInputs, trainingOutputs)
}


fun buildNeuralNetwork(): MultiLayerNetwork {
    val conf = NeuralNetConfiguration.Builder()
            .seed(123456)
            .iterations(10000)
            .weightInit(WeightInit.DISTRIBUTION)
            .learningRate(0.1)
            .list()
            .layer(0, DenseLayer.Builder().nIn(30).nOut(20)
                    .build())
            .layer(1, DenseLayer.Builder().nIn(20).nOut(10)
                    .build())
            .layer(2, DenseLayer.Builder().nIn(10).nOut(8)
                    .build())
            .layer(3, OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                    .activation(Activation.IDENTITY)
                    .nIn(8).nOut(2).build())
            .backprop(true)
            .pretrain(false)
            .build()
    val myNetwork = MultiLayerNetwork(conf)
    myNetwork.init()
    return myNetwork
}


fun main(args: Array<String>) {
    val basePath = "/home/fei/sad0027"

    val outputFile = "$basePath/network2.serialized"

    println("loading")
    val files = arrayListOf(
            "$basePath/Measuring275592786149260.csv",
            "$basePath/Measuring276282021059848.csv",
            "$basePath/Measuring276888709065493.csv"
    )
    val d = loadData(files)

    println("preparing")
    //val rows = firstTrainingSet(d)
    val rows = secondTrainingSet(d)

    val dataSet = convertToDataSet(rows)

    println("build network")

    val network = if (File(outputFile).exists())
        ModelSerializer.restoreMultiLayerNetwork(outputFile)
    else
        buildNeuralNetwork()

    network.setListeners(object : IterationListener {
        var invoked = false

        override fun invoked() = invoked

        override fun invoke() {
            invoked = true
        }

        override fun iterationDone(model: Model, iteration: Int) {
            if (iteration % 100 == 0) {
                val result = model.score()
                println("Score at iteration $iteration is $result")
            }
        }

    })
    (1..1000).map {
        println("learning")
        network.fit(dataSet)

        println("saving")
        ModelSerializer.writeModel(network, outputFile, true)
    }

}
